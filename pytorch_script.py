import torch

if __name__ == '__main__':
    print(torch.__version__)
    print('is cuda available: ', torch.cuda.is_available())
