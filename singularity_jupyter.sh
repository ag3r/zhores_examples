set -e
module load gpu/cuda-10.0
SINGULARITYENV_JUPYTER_RUNTIME_DIR=/tmp/$USER/jupyter-rtd srun singularity exec --nv --writable-tmpfs -B /gpfs/gpfs0/ukb_data,/gpfs/gpfs0/$USER ~/ukb_ml/containers/ukb.sif jupyter-notebook --no-browser --ip=0.0.0.0