# Zhores small example repo

## How to run scripts

Simple submission of shell script, all slurm arguments are already in script:

```sbatch pytorch_script.sh```

By default, slurm catches stdout and stderr in slurm-{$job-number}.out. If you want control over filenames, you should use 
```
mkdir output
sbatch --error "output/job.err" --output "output/job.out"  pytorch_script.sh
```

Interactive work can be performed with ```salloc```. 
You allocate resources and then can launch everything in the same terminal

```
salloc -p gpu_debug -N 1 -n 1 -t 0:30:00 --cpus-per-task 1 --mem 8000 --gpus 1
```



## Cluster info

```
###############################################################################
                    Queues assignment            Priority    MaxTime MaxNodes
cpu/gpu/mem_big      parallel calculations        Normal        6d    22/12/4
cpu/gpu/mem_small    short term calculations      High          24h   22/12/4
cpu/gpu/mem_debug    test queue, code precheck    Max           30m      2
gpu_devel            code development, Jupyter    Normal        12h      2
htc                  high throughput              Minimal       24h      1
###############################################################################
Name          MaxTRESPU   MaxJobs        MinTRES
----------  ------------- -------     -------------
gpu_devel    --gpus=1       2
gpu_big      --gpus=32      12          --gpus=1
gpu_small    --gpus=32      12          --gpus=1
cpu_smal      cpu=528       16
cpu_big       cpu=288       16
mem_big       cpu=160
mem_small     cpu=400
###############################################################################
node type    node names CPUs  Cores   GPUs   Memory default      Total avail
cpu          cn[01-44]   2     24      -          6GB                160GB
mem          ct[01-04]   4     80      -          6GB                720GB
mem          ct[04-08]   4     80      -          6GB               1500GB
mem          ct[09-10]   4     80      -          6GB               3000GB
gpu          gn[01-25]   2     36      4          6GB                360GB
devel        vt[01-14]   1      4      1          6GB                 26GB
###############################################################################
JupytherLab, single entry point with TensorFlow and PyTorch:
  http://10.5.1.1:8000/
 * Windows Internet Explorer is not supported.
###############################################################################
Docs and other information about cluster:
  https://hpc.skoltech.ru/supercomputers/
Need help:
  hpc_support@skoltech.ru
###############################################################################
Storage quotas: 2TB, grace period 7 days with limit 5TB.
limits on the GPFS part could be exteneded by request.
###############################################################################
  http://10.5.1.1:8000/
 * Windows Internet Explorer is not supported.
###############################################################################
Docs and other information about cluster:
  https://hpc.skoltech.ru/supercomputers/
Need help:
  hpc_support@skoltech.ru
###############################################################################
Storage quotas: 2TB, grace period 7 days with limit 5TB.
limits on the GPFS part could be extended by request.
###############################################################################
```
