#!/bin/bash

#SBATCH --job-name test
#SBATCH --partition gpu_debug
#SBATCH --nodes 1
#SBATCH --cpus-per-task 1
#SBATCH --time 00:30:00 # hh:mm:ss, walltime
#SBATCH --mem 8000
#SBATCH --gpus 1

cd /gpfs/data/home/a.medvedev/ukb_ml

singularity exec --nv -B /gpfs/gpfs0/ukb_data:/gpfs/gpfs0/ukb_data containers/ukbnew.sif python -u ukb_ml/scripts/pipeline/preprocess.py

