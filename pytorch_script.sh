#!/bin/bash
#SBATCH --job-name test
#SBATCH --partition gpu_debug
#SBATCH --nodes 1
#SBATCH --cpus-per-task 1
#SBATCH --time 00:20:00 # hh:mm:ss, walltime
#SBATCH --mem 8000
#SBATCH --gpus 1

echo 'i was launched'
#pwd
module load gpu/cuda-10.2
module load python/3.8
module load python/pytorch-1.6.0
nvidia-smi
python3 -u pytorch_script.py
